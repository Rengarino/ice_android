import sys, Ice
import Demo
import os
import eyed3
import vlc
import time
import re 
import signal

idClient = 0
port = 5100

class SongManager(Demo.SongManager):
	def __init__(self):
		self.music = bytes() #Tableau de bytes
		self.songs = []
		self.song_folder = song_folder = os.path.expanduser(os.path.abspath(os.getcwd())+"/rapfr")
		self.vlc = vlc.Instance()

	def send(self, bytes, current=None):
		self.music += bytes

	def save(self, name, current=None):
		file = open(self.song_folder+"/"+name+".mp3", "wb") #write binary
		file.write(self.music)
		self.music = bytes() #On remet le tableau à 0	
		file.close()
		print("Sauvegarde du fichier")

	def delete(self, name, current=None):
		if os.path.isfile(self.song_folder+"/"+name+".mp3"):
			os.remove(self.song_folder+"/"+name+".mp3")
			print("La musique a bien ete supprimee")
		else:
			print("La musique demandée n'existe pas")

	def modify(self, name, newName, current=None):
		if os.path.isfile(self.song_folder+"/"+name+".mp3"):
			n = name+".mp3"
			n2 = newName+".mp3"
			os.rename(self.song_folder+"/"+n, self.song_folder+"/"+n2)
		else:
			print("La musique n'existe pas")

	def searchByName(self, name, current=None):
		files = os.listdir(self.song_folder)
		self.songs = []
		for f in files:
			abs_location = os.path.join(self.song_folder, f)
			song_info = eyed3.load(abs_location)
			if re.search(name, song_info.tag.title, re.IGNORECASE) or re.search(name, f, re.IGNORECASE):
				self.songs.append(f)
		return self.songs	

	def searchByArtist(self, artist, current=None):
		files = os.listdir(self.song_folder)
		self.songs = []
		for f in files:
			abs_location = os.path.join(self.song_folder, f)
			song_info = eyed3.load(abs_location)
			if  re.search(artist, f, re.IGNORECASE) or re.search(artist, song_info.tag.artist, re.IGNORECASE) or re.search(artist, song_info.tag.title, re.IGNORECASE):
				self.songs.append(f)
		return self.songs

	def getAllSongs(self, current=None):
		files = os.listdir(self.song_folder)
		self.songs = []
		for f in files: 
			abs_location = os.path.join(self.song_folder, f)
			song_info = eyed3.load(abs_location)
			self.songs.append(f) 
		return self.songs

	"""def playWithVLC(self, name, current=None):
		if os.path.isfile(self.song_folder+"/"+name):
			instance = vlc.Instance()
			player = instance.media_player_new()
			#print(song_folder+"/"+name+".mp3")
			media = instance.media_new(self.song_folder+"/"+name+".mp3")
			player.set_media(media)

			player.play()
			time.sleep(10)
		else: 
			print("La musique demandee n'existe pas")"""

	def streamWithVLC(self, name, port, current=None):
		if os.path.isfile(self.song_folder+"/"+name):
			player = self.vlc.media_player_new()
			#print(song_folder+"/"+name+".mp3")
			#media = instance.media_new(song_folder+"/"+name+".mp3")
			title = self.song_folder+"/"+name
			#broadcastName = "Stream client numero"+number

			broadcastName = "SOUP"
			stream = "#transcode{acodec=mp3,ab=128,channels=2,samplerate=44100}:http{dst=:"+str(port)+"/stream.mp3}"

			if(self.vlc.vlm_add_broadcast(broadcastName,title, stream,0, [], True, False)!=0):
				print("Erreur de broadcast");
			
			else: 
				self.vlc.vlm_play_media(broadcastName)

		else: 
			print("La musique demandee n'existe pas")

	def pauseVLC(self, broadcast, current=None):
		self.vlc.vlm_pause_media(broadcast)

	def stopVLC(self, broadcast, current=None):
		print("Arrêt du broadcast")
		self.vlc.vlm_stop_media(broadcast)
		self.vlc.vlm_del_media(broadcast) #Delete le stream attaché à l'instance vlc pour pouvoir en recréer un

class Connect(Demo.Connect):
	def getIdClient(self, current=None):
		global idClient 
		idClient = idClient+1
		print("Client numero "+str(idClient)+" connecte")
		return idClient

	def getPort(self, current=None):
		global port
		port = port +1
		return port

	def getStreamStr(self, current=None):
		global port
		return "http://localhost:"+str(port)+"/stream.mp3";

with Ice.initialize(sys.argv) as communicator:

    #
    # Install a signal handler to shutdown the communicator on Ctrl-C
    #
    signal.signal(signal.SIGINT, lambda signum, frame: communicator.shutdown())

    #
    # The communicator initialization removes all Ice-related arguments from argv
    #
    if len(sys.argv) > 1:
        print(sys.argv[0] + ": too many arguments")
        sys.exit(1)

    properties = communicator.getProperties()
    adapter = communicator.createObjectAdapter("ServerRapFR")
    id = Ice.stringToIdentity(properties.getProperty("connectRapFRIdentity"))
    adapter.add(Connect(), id)
    id = Ice.stringToIdentity(properties.getProperty("songManagerRapFRIdentity"))
    adapter.add(SongManager(), id)
    adapter.activate()
    communicator.waitForShutdown()
