package com.example.soupify;

import androidx.appcompat.app.AppCompatActivity;
import com.zeroc.Ice.Communicator;
import com.zeroc.Ice.InitializationData;
import com.zeroc.Ice.ObjectPrx;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import Demo.ConnectPrx;

public class MainActivity extends AppCompatActivity {
    private Connector cl = null;
    private Communicator communicator = null;
    private String[] args;
    ConnectPrx connect = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //connectToServer();
        //cl.getServer().printString("HEY");

        connectToRegistry();
        getConnectInterface();
        //communicator.getProperties().setProperty("Ice.Default.Package", "com.example.soupify");
        //Definition du proxy IceGrid
        //communicator.getProperties().setProperty("Ice.Default.Locator", "DemoIceGrid/Locator:default -h 192.168.1.20 -p 10000");

    }

    private void getConnectInterface() {
        try {
            connect = ConnectPrx.checkedCast(communicator.stringToProxy("connect"));
        }
        catch (Exception e) {
            Log.e("Interface connect", e.toString());
        }
        if(connect == null) {
            System.err.println("Objet Demo::ConnectPrx non trouvé");
        }
        else {
            TextView connected = findViewById(R.id.textViewConnect);
            connected.setText("Vous êtes connecté en tant que Client n°" + connect.getIdClient());
        }
    }

    private void connectToRegistry() {
        try {
            InitializationData initData = new InitializationData();
            initData.properties = com.zeroc.Ice.Util.createProperties();
            initData.properties.setProperty("Ice.Default.Locator", "DemoIceGrid/Locator:default -h 192.168.1.20 -p 10000");
            communicator = com.zeroc.Ice.Util.initialize(initData);
        }
        catch (Exception e) {
            Log.e("Connexion Ice", e.toString());
        }
    }

    /*private void connectToServer() {
        if(cl == null) {
            new ConnectorLoader().run(communicator, this);
        }
    }

    private class ConnectorLoader extends Thread {
        public void run(Communicator communicator, Activity act) {
            cl = new Connector(communicator, act);
        }
    }*/
}
