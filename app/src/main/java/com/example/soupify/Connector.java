package com.example.soupify;

import com.zeroc.Ice.Communicator;
import android.app.Activity;
import android.util.Log;
import com.zeroc.Ice.ObjectPrx;


public class Connector {
    private Communicator communicator = null;
    private Activity activity = null;
    private boolean isConnected = false;
    private String address = "192.168.1.20";
    private String port = "10000";

    public Connector(Communicator communicator, final Activity activity) {
        this.communicator = communicator;
        this.activity = activity;
        initServer();

    }


    private void initServer() {
        if (!isConnected)
            return;
        try {
            ObjectPrx base = communicator.stringToProxy("DefaultServer:tcp -h " + address + " -p " + port);
            //server = PrinterPrx.checkedCast(base);
            isConnected = true;
        } catch (Exception e) {
            Log.e("DefaultServer", e.toString());
        }
    }
}
