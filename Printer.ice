#pragma once
module Demo {
	sequence<byte> ByteSeq;
    ["python:list"] sequence<string> SongList;

    interface Connect {
        int getIdClient();
        int getPort();
        string getStreamStr();
    };

    interface SongManager {
    	void send(ByteSeq bytes);
    	void save(string name);
    	void delete(string name);
    	void modify(string name, string newName);

    	SongList searchByName(string name);
    	SongList searchByArtist(string artiste);
        SongList getAllSongs();

    	void playWithVLC(string name);
        void streamWithVLC(string name, int port);
        void pauseVLC(string broadcast);
        void stopVLC(string broadcast);
    };
};